type 'a ty =
  | Int: int ty
  | String: string ty
  | List: 'a ty -> 'a list ty
  | Pair: ('a ty * 'b ty) -> ('a * 'b) ty
  | Record: ('a, 'a) record -> 'a ty
 
and (_, _) record =
| Build : 'b -> ('b, 'r) record
| Field : ('a -> 'builder, 'r) record * string * 'a ty * ('r -> 'a) -> ('builder, 'r) record
;;


type my_record  =
    {
     a: int;
     b: string list;
    }
 
let my_record = Record (
  Field (Field (Build (fun a b -> {a;b}),
                "a", Int, (fun {a} -> a)),
         "b", List String, (fun {b} -> b))
)

let rec identity : type a . a ty -> a -> a = function
  | Int -> (fun n -> n+0)
  | String -> (fun s -> s^"")
  | List t -> List.map (identity t)
  | Pair (ta, tb) -> (fun (a, b) -> identity ta a, identity tb b)
  | Record recty -> fun record ->
    let rec fid : type b . (b, a) record -> (b -> a) -> a = function
      | Build b -> (fun k -> k b)
      | Field (rest, _name, ty, read) ->
        let field = identity ty (read record) in
        (fun k -> fid rest (fun b -> k (b field)))
    in fid recty (fun r -> r)
